#pragma once
#include "StudentInterface.h"

class Student : public StudentInterface {
	private:
		unsigned long long int ID;
		string name;
		string address;
		string phone;
		string GPA;
	public:
		Student();
		~Student();
		unsigned long long int getID();
		string getName();
		string getGPA();
		void addGPA(double classGrade);
		string toString();	
};
