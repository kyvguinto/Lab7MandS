#pragma once
#include <iostream>
#include "GPAInterface.h"


class GPA : public GPAInterface {
	private:
	
	public:
		GPA();
		~GPA();
		map<unsigned long long int, StudentInterface*> getMap();
		set<StudentInterface*, Comparator> getSet();
		bool importStudents(string mapFileName, string setFileName);
		bool importGrades(string fileName);
		string querySet(string fileName);
		string queryMap(string fileName);
		void clear();
};
